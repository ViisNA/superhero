package com.strokova.superhero.api;

import com.strokova.superhero.api.model.SuperheroRequest;
import com.strokova.superhero.api.model.SuperheroResponse;
import com.strokova.superhero.repository.model.Publisher;
import com.strokova.superhero.repository.model.Skill;
import org.junit.Test;
import org.springframework.http.MediaType;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class SuperheroControllerTest extends AbstractControllerTest {

    @Test
    public void getSuperhero() throws Exception {
        long id = 1L;
        String name = "Dr. Banner";
        String pseudonym = "The Hulk";
        Publisher publisher = Publisher.Marvel;
        LocalDate date = LocalDate.now();
        Set<Skill> skills = new HashSet<>(Arrays.asList(Skill.NIGHT_VISION, Skill.DUPLICATION));
        Set<Long> allyIds = new HashSet<>(Arrays.asList(2L, 9L, 42L));
        SuperheroResponse heroResponse = new SuperheroResponse(id, name, pseudonym, publisher, date, skills, allyIds);
        given(superheroService.find(1L)).willReturn(heroResponse);
        mvc.perform(get("/v1/superhero/{id}", id).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8_VALUE))
                .andReturn();
    }

    @Test
    public void getSuperheroNotExisting() throws Exception {
        given(superheroService.find(42L)).willThrow(new EntityNotFoundException());
        mvc.perform(get("/v1/superhero/{id}", 42L))
                .andExpect(status().isNoContent());
    }

    @Test
    public void addSuperhero() throws Exception {
        long id = 6L;
        String name = "Barbara Gordon";
        String pseudonym = "Batgirl";
        Publisher publisher = Publisher.DC;
        LocalDate date = LocalDate.now();
        Set<Skill> skills = new HashSet<>();
        Set<Long> allyIds = new HashSet<>();
        SuperheroResponse heroResponse = new SuperheroResponse(id, name, pseudonym, publisher, date, skills, allyIds);
        given(superheroService.add(name, pseudonym, publisher)).willReturn(heroResponse);
        mvc.perform(post("/v1/superhero").contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(new SuperheroRequest(name, pseudonym, publisher))))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8_VALUE))
                .andReturn();
    }

}
