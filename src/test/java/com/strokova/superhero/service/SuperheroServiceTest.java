package com.strokova.superhero.service;

import com.strokova.superhero.api.exception.NoSuchEntityException;
import com.strokova.superhero.api.model.SuperheroResponse;
import com.strokova.superhero.repository.model.Publisher;
import com.strokova.superhero.repository.model.Skill;
import com.strokova.superhero.repository.model.Superhero;
import org.junit.Test;
import org.springframework.dao.DataIntegrityViolationException;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@Transactional
public class SuperheroServiceTest extends AbstractServiceTest {

    private long heroId1 = 1L;
    private long heroId2 = 2L;
    private long heroId3 = 3L;
    private long heroDeleteId = 4L;
    private long heroGetId1 = 5L;
    private long heroGetId2 = 6L;
    private long heroGetId3 = 7L;

    @Test
    public void addSuperheroFullData() {
        String name = "Logan";
        String pseudonym = "Wolverine";
        Publisher publisher = Publisher.Marvel;
        SuperheroResponse hero = superheroService.add(name, pseudonym, publisher);
        assertNotNull(hero.getId());
        assertEquals(name, hero.getName());
        assertEquals(pseudonym, hero.getPseudonym());
        assertEquals(publisher, hero.getPublisher());
        assertNotNull(hero.getDate());
        assertTrue(hero.getAllyIds().isEmpty());
        assertTrue(hero.getSkills().isEmpty());
    }

    @Test
    public void addSuperheroNoPseudonym() {
        String name = "Batman";
        Publisher publisher = Publisher.DC;
        SuperheroResponse hero = superheroService.add(name, null, publisher);
        assertNotNull(hero.getId());
        assertNotNull(hero.getDate());
        assertNull(hero.getPseudonym());
        assertEquals(name, hero.getName());
        assertEquals(publisher, hero.getPublisher());
        assertTrue(hero.getAllyIds().isEmpty());
        assertTrue(hero.getSkills().isEmpty());
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void addSuperheroDuplicateName() {
        assertNotNull(superheroService.add("my name", "my pseudo", Publisher.Marvel).getId());
        superheroService.add("my name", "my pseudo", Publisher.Marvel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addSuperheroNoName() {
        superheroService.add(null, "pseudonym", Publisher.Marvel);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addSuperheroNoPublisher() {
        superheroService.add("name", "pseudonym", null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addSuperheroNoNamePublisher() {
        superheroService.add(null, "pseudonym", null);
    }

    @Test
    public void editSuperheroAll() {
        String newName = "new name 1";
        String newPseudonym = "new pseudo 1";
        Publisher newPublisher = Publisher.Marvel;
        superheroService.edit(heroId1, newName, newPseudonym, newPublisher);
        Superhero edited = superheroRepository.findById(heroId1).get();
        assertEquals(newName, edited.getName());
        assertEquals(newPseudonym, edited.getPseudonym());
        assertEquals(newPublisher, edited.getPublisher());
    }

    @Test
    public void editSuperheroPseudonym() {
        superheroService.edit(heroId2, null, null, null);
        Superhero edited = superheroRepository.findById(heroId2).get();
        assertNotNull(edited.getName());
        assertNotNull(edited.getPublisher());
        assertNull(edited.getPseudonym());

        superheroService.edit(heroId2, null, "new 2", null);
        assertNotNull(edited.getName());
        assertNotNull(edited.getPublisher());
        assertEquals("new 2", edited.getPseudonym());

        superheroService.edit(heroId2, null, " ", null);
        assertNotNull(edited.getName());
        assertNotNull(edited.getPublisher());
        assertNull(edited.getPseudonym());
    }

    @Test
    public void editSuperheroEmptyNamePublisher() {
        superheroService.edit(heroId3, "", null, null);
        Superhero edited = superheroRepository.findById(heroId3).get();
        assertEquals("name 3", edited.getName());
        assertEquals(Publisher.DC, edited.getPublisher());

        superheroService.edit(heroId3, null, null, null);
        assertEquals("name 3", edited.getName());
        assertEquals(Publisher.DC, edited.getPublisher());
    }

    @Test(expected = IllegalArgumentException.class)
    public void editSuperheroNoId() {
        superheroService.edit(null, "name", "pseudonym", Publisher.DC);
    }

    @Test(expected = NoSuchEntityException.class)
    public void editSuperheroNotExistingId() {
        superheroService.edit(142L, "name", "pseudonym", Publisher.DC);
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteSuperheroNoId() {
        superheroService.deleteSuperhero(null);
    }

    @Test
    public void deleteSuperhero() {
        assertTrue(superheroRepository.findById(heroDeleteId).isPresent());
        assertTrue(superheroRepository.findById(heroId1).get()
                .getAllies().stream().map(Superhero::getSuperheroId)
                .collect(Collectors.toSet()).contains(heroDeleteId));
        superheroService.deleteSuperhero(heroDeleteId);
        assertFalse(superheroRepository.findById(heroDeleteId).isPresent());
        em.flush();
        superheroRepository.findById(heroId1).ifPresent(hero1 -> {
            em.refresh(hero1);
            assertFalse(hero1.getAllies().stream().map(Superhero::getSuperheroId)
                    .collect(Collectors.toSet()).contains(heroDeleteId));
        });
    }

    @Test
    public void addSkills() {
        Set<Skill> skillSet1 = new HashSet<>(Arrays.asList(Skill.FIRE_BALL, Skill.DUPLICATION));
        superheroService.addSkills(heroId1, skillSet1);
        Set<Skill> skills1 = superheroRepository.findById(heroId1).get().getSkills();
        assertEquals(2, skills1.size());
        assertTrue(skills1.containsAll(skillSet1));

        Set<Skill> skillSet2 = new HashSet<>(Arrays.asList(Skill.INVISIBILITY, Skill.DUPLICATION));
        superheroService.addSkills(heroId1, skillSet2);
        Set<Skill> skills2 = superheroRepository.findById(heroId1).get().getSkills();
        assertEquals(3, skills2.size());
        assertTrue(skills2.containsAll(skillSet1));
        assertTrue(skills2.contains(Skill.INVISIBILITY));
    }

    @Test
    public void deleteSkills() {
        Set<Skill> originalSkills = superheroRepository.findById(heroId2).get().getSkills();
        int originalSkillsSize = originalSkills.size();
        Set<Skill> skillsToDelete = new HashSet<>(Arrays.asList(Skill.NIGHT_VISION, Skill.DUPLICATION));
        assertTrue(originalSkills.containsAll(skillsToDelete));

        superheroService.deleteSkills(heroId2, skillsToDelete);
        Set<Skill> editedSkills = superheroRepository.findById(heroId2).get().getSkills();
        assertEquals(originalSkillsSize - skillsToDelete.size(), editedSkills.size());
        skillsToDelete.forEach(skill -> assertFalse(editedSkills.contains(skill)));
    }

    @Test
    public void addAllies() {
        assertFalse(superheroRepository.findById(heroId1).get().getAllies()
                .stream().map(Superhero::getSuperheroId)
                .collect(Collectors.toSet()).contains(heroId2));
        assertFalse(superheroRepository.findById(heroId2).get().getAllies()
                .stream().map(Superhero::getSuperheroId)
                .collect(Collectors.toSet()).contains(heroId1));

        superheroService.addAllies(heroId2, Collections.singleton(heroId1));

        assertTrue(superheroRepository.findById(heroId2).get().getAllies()
                .stream().map(Superhero::getSuperheroId)
                .collect(Collectors.toSet()).contains(heroId1));
        em.flush();
        superheroRepository.findById(heroId1).ifPresent(hero1 -> {
            em.refresh(hero1);
            assertTrue(hero1.getAllies().stream().map(Superhero::getSuperheroId)
                    .collect(Collectors.toSet()).contains(heroId2));
        });
    }

    @Test
    public void deleteAllies() {
        assertTrue(superheroRepository.findById(heroId1).get().getAllies()
                .stream().map(Superhero::getSuperheroId)
                .collect(Collectors.toSet()).contains(heroId3));
        assertTrue(superheroRepository.findById(heroId3).get().getAllies()
                .stream().map(Superhero::getSuperheroId)
                .collect(Collectors.toSet()).contains(heroId1));

        superheroService.deleteAllies(heroId3, Collections.singleton(heroId1));

        assertFalse(superheroRepository.findById(heroId3).get().getAllies()
                .stream().map(Superhero::getSuperheroId)
                .collect(Collectors.toSet()).contains(heroId1));
        em.flush();
        superheroRepository.findById(heroId1).ifPresent(hero1 -> {
            em.refresh(hero1);
            assertFalse(hero1.getAllies()
                    .stream().map(Superhero::getSuperheroId)
                    .collect(Collectors.toSet()).contains(heroId3));
        });
    }

    @Test
    public void getSuperhero() {
        SuperheroResponse response = superheroService.find(heroGetId1);
        assertEquals(heroGetId1, response.getId().longValue());
        assertEquals("a hero 1", response.getName());
        assertEquals("Superman", response.getPseudonym());
        assertEquals(Publisher.DC, response.getPublisher());
        assertNotNull(response.getDate());
        assertEquals(2, response.getSkills().size());
        Set<Long> allyIds = response.getAllyIds();
        assertEquals(1, allyIds.size());
        assertTrue(allyIds.contains(heroGetId2));
    }

    @Test
    public void getSuperheroList() {
        List<SuperheroResponse> page3 = superheroService.findPage(0, 3);
        assertEquals(3, page3.size());
        SuperheroResponse hero1 = page3.get(0);
        assertEquals(heroGetId1, hero1.getId().longValue());
        assertFalse(hero1.getSkills().isEmpty());
        assertEquals(1, hero1.getAllyIds().size());
        assertEquals(heroGetId2, page3.get(1).getId().longValue());
        assertEquals(heroGetId3, page3.get(2).getId().longValue());
    }

}
