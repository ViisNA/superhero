package com.strokova.superhero.service;

import com.strokova.superhero.repository.SuperheroRepository;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

@RunWith(SpringRunner.class)
@SpringBootTest
public abstract class AbstractServiceTest {

    @Autowired
    SuperheroRepository superheroRepository;

    @Autowired
    SuperheroService superheroService;

    @Autowired
    EntityManager em;

}
