package com.strokova.superhero.api;

import com.strokova.superhero.api.model.SuperheroRequest;
import com.strokova.superhero.api.model.SuperheroResponse;
import com.strokova.superhero.repository.model.Skill;
import com.strokova.superhero.service.SuperheroService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

@RestController
@RequestMapping("/v1/superhero")
public class SuperheroController {

    private static final Logger LOG = Logger.getLogger(SuperheroController.class.getName());

    private final SuperheroService superheroService;

    public SuperheroController(SuperheroService superheroService) {
        this.superheroService = superheroService;
    }

    @GetMapping("/{id}")
    public SuperheroResponse getSuperhero(
            @PathVariable("id") Long id) {
        LOG.info(String.format("[getSuperhero] id = %s.", id));
        return superheroService.find(id);
    }

    @GetMapping("/superheroes")
    public List<SuperheroResponse> getSuperheroes(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "size", required = false) Integer size) {
        LOG.info(String.format("[getSuperheroes] page = %s, size = %s.", page, size));
        return superheroService.findPage(page, size);
    }

    @PostMapping
    public SuperheroResponse addSuperhero(
            @RequestBody SuperheroRequest request) {
        LOG.info(String.format("[addSuperhero] %s.", request));
        return superheroService.add(request.getName(), request.getPseudonym(), request.getPublisher());
    }

    @PatchMapping("/{id}")
    public SuperheroResponse editSuperhero(
            @PathVariable("id") Long id,
            @RequestBody SuperheroRequest request) {
        LOG.info(String.format("[editSuperhero] %s.", request));
        return superheroService.edit(id, request.getName(), request.getPseudonym(), request.getPublisher());
    }

    @DeleteMapping("/{id}")
    public void deleteSuperhero(
            @PathVariable("id") Long id) {
        LOG.info(String.format("[deleteSuperhero] id = %s.", id));
        superheroService.deleteSuperhero(id);
    }

    @PatchMapping("/{id}/skills")
    public SuperheroResponse addSuperheroSkills(
            @PathVariable("id") Long id,
            @RequestBody Set<Skill> skills) {
        LOG.info(String.format("[addSuperheroSkills] id = %s, skills = %s.", id, skills));
        return superheroService.addSkills(id, skills);
    }

    @DeleteMapping("/{id}/skills")
    public SuperheroResponse deleteSuperheroSkills(
            @PathVariable("id") Long id,
            @RequestBody Set<Skill> skills) {
        LOG.info(String.format("[deleteSuperheroSkills] id = %s, skills = %s.", id, skills));
        return superheroService.deleteSkills(id, skills);
    }

    @PatchMapping("/{id}/allies")
    public SuperheroResponse addSuperheroAllies(
            @PathVariable("id") Long id,
            @RequestBody Set<Long> allyIds) {
        LOG.info(String.format("[addSuperheroAllies] id = %s, allyIds = %s.", id, allyIds));
        return superheroService.addAllies(id, allyIds);
    }

    @DeleteMapping("/{id}/allies")
    public SuperheroResponse deleteSuperheroAllies(
            @PathVariable("id") Long id,
            @RequestBody Set<Long> allyIds) {
        LOG.info(String.format("[deleteSuperheroAllies] id = %s, allyIds = %s.", id, allyIds));
        return superheroService.deleteAllies(id, allyIds);
    }

}
