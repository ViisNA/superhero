package com.strokova.superhero.api.model;

import com.strokova.superhero.repository.model.Publisher;
import com.strokova.superhero.repository.model.Skill;
import com.strokova.superhero.repository.model.Superhero;

import java.time.LocalDate;
import java.util.Set;
import java.util.stream.Collectors;

public class SuperheroResponse {

    private final Long id;
    private final String name;
    private final String pseudonym;
    private final Publisher publisher;
    private final LocalDate date;
    private final Set<Skill> skills;
    private final Set<Long> allyIds;

    public SuperheroResponse(Superhero superhero) {
        this.id = superhero.getSuperheroId();
        this.name = superhero.getName();
        this.pseudonym = superhero.getPseudonym();
        this.publisher = superhero.getPublisher();
        this.date = superhero.getCreationDate();
        this.skills = superhero.getSkills();
        this.allyIds = superhero.getAllies().stream()
                .map(Superhero::getSuperheroId)
                .collect(Collectors.toSet());
    }

    public SuperheroResponse(Long id, String name, String pseudonym, Publisher publisher, LocalDate date,
                             Set<Skill> skills, Set<Long> allyIds) {
        this.id = id;
        this.name = name;
        this.pseudonym = pseudonym;
        this.publisher = publisher;
        this.date = date;
        this.skills = skills;
        this.allyIds = allyIds;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPseudonym() {
        return pseudonym;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public LocalDate getDate() {
        return date;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public Set<Long> getAllyIds() {
        return allyIds;
    }

}
