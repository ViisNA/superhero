package com.strokova.superhero.api.model;

import com.strokova.superhero.repository.model.Publisher;

public class SuperheroRequest {

    private String name;
    private String pseudonym;
    private Publisher publisher;

    public SuperheroRequest() {

    }

    public SuperheroRequest(String name, String pseudonym, Publisher publisher) {
        this.name = name;
        this.pseudonym = pseudonym;
        this.publisher = publisher;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPseudonym() {
        return pseudonym;
    }

    public void setPseudonym(String pseudonym) {
        this.pseudonym = pseudonym;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    @Override
    public String toString() {
        return "SuperheroCreateRequest {" +
                "name='" + name + '\'' +
                ", pseudonym='" + pseudonym + '\'' +
                ", publisher=" + publisher +
                '}';
    }

}
