package com.strokova.superhero.api.exception;

import com.strokova.superhero.api.model.ErrorResponse;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.util.logging.Logger;

@ControllerAdvice
public class ApplicationExceptionHandler {

    private static final Logger LOG = Logger.getLogger(ApplicationExceptionHandler.class.getName());

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ResponseBody
    public ErrorResponse handle204Exception(RuntimeException ex, HttpServletRequest request) {
        return makeErrorResponse(ex, request, HttpStatus.NO_CONTENT.toString());
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse handle400Exception(RuntimeException ex, HttpServletRequest request) {
        return makeErrorResponse(ex, request, HttpStatus.BAD_REQUEST.toString());
    }

    @ExceptionHandler(NoSuchEntityException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ErrorResponse handle404Exception(RuntimeException ex, HttpServletRequest request) {
        return makeErrorResponse(ex, request, HttpStatus.NOT_FOUND.toString());
    }

    private ErrorResponse makeErrorResponse(Exception ex, HttpServletRequest request, String status) {
        LOG.severe(String.format("URI = %s : %s", request.getRequestURI(), ex));
        return new ErrorResponse(ex.getMessage(), status);
    }

}
