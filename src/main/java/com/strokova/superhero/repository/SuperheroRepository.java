package com.strokova.superhero.repository;

import com.strokova.superhero.repository.model.Superhero;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SuperheroRepository extends PagingAndSortingRepository<Superhero, Long> {
}
