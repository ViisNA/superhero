package com.strokova.superhero.repository.model;

public enum Publisher {

    DC, Marvel

}
