package com.strokova.superhero.repository.model;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Superhero {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "superhero_id")
    private Long superheroId;

    @Column(nullable = false, unique = true)
    private String name;

    private String pseudonym;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Publisher publisher;

    @Column(nullable = false)
    private LocalDate creationDate;

    @Enumerated(EnumType.STRING)
    @ElementCollection
    @CollectionTable(name = "superhero_skill", joinColumns = @JoinColumn(name = "superhero_id"))
    private Set<Skill> skills = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "superhero_ally", joinColumns = @JoinColumn(name = "superhero_id"),
            inverseJoinColumns = @JoinColumn(name = "ally_id", referencedColumnName = "superhero_id"))
    private Set<Superhero> allies = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "superhero_ally", joinColumns = @JoinColumn(name = "ally_id", referencedColumnName = "superhero_id"),
            inverseJoinColumns = @JoinColumn(name = "superhero_id"))
    private Set<Superhero> implicitAllies = new HashSet<>();

    public Superhero() {
    }

    public Superhero(String name, String pseudonym, Publisher publisher) {
        this.name = name;
        this.pseudonym = pseudonym;
        this.publisher = publisher;
        this.creationDate = LocalDate.now();
    }

    public Long getSuperheroId() {
        return superheroId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPseudonym() {
        return pseudonym;
    }

    public void setPseudonym(String pseudonym) {
        this.pseudonym = pseudonym;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public Set<Superhero> getAllies() {
        return allies;
    }

    public Set<Superhero> getImplicitAllies() {
        return implicitAllies;
    }

    @Override
    public String toString() {
        return "Superhero {" +
                "superheroId=" + superheroId +
                ", name='" + name + '\'' +
                ", pseudonym='" + pseudonym + '\'' +
                ", publisher=" + publisher +
                ", creationDate=" + creationDate +
                ", skills=" + skills +
                ", allies=" + allies.stream().map(Superhero::getName).collect(Collectors.joining(",")) +
                ", implicitAllies=" + implicitAllies.stream().map(Superhero::getName).collect(Collectors.joining(",")) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Superhero superhero = (Superhero) o;
        return Objects.equals(superheroId, superhero.superheroId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(superheroId);
    }

}
