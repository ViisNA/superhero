package com.strokova.superhero.repository.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Skill {

    @JsonProperty("invisibility")
    INVISIBILITY,

    @JsonProperty("duplication")
    DUPLICATION,

    @JsonProperty("fire_ball")
    FIRE_BALL,

    @JsonProperty("poison")
    POISON,

    @JsonProperty("night_vision")
    NIGHT_VISION

}
