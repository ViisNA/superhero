package com.strokova.superhero.util;

public final class Utils {

    private Utils() {
    }

    public static void checkParam(Object param, String errorMessage) {
        if (param == null) {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    public static void checkStringParam(String param, String errorMessage) {
        if (isEmptyString(param)) {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    public static boolean isEmptyString(String param) {
        return param == null || param.trim().isEmpty();
    }

}
