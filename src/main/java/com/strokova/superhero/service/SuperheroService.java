package com.strokova.superhero.service;

import com.strokova.superhero.api.exception.NoSuchEntityException;
import com.strokova.superhero.api.model.SuperheroResponse;
import com.strokova.superhero.repository.SuperheroRepository;
import com.strokova.superhero.repository.model.Publisher;
import com.strokova.superhero.repository.model.Skill;
import com.strokova.superhero.repository.model.Superhero;
import com.strokova.superhero.util.Utils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class SuperheroService {

    private static final int DEFAULT_PAGE_NUM = 0;
    private static final int DEFAULT_PAGE_SIZE = 15;

    private final SuperheroRepository superheroRepository;

    public SuperheroService(SuperheroRepository superheroRepository) {
        this.superheroRepository = superheroRepository;
    }

    public SuperheroResponse add(String name, String pseudonym, Publisher publisher) {
        Utils.checkStringParam(name, "The name cannot be empty.");
        Utils.checkParam(publisher, "The publisher cannot be empty.");

        Superhero saved = superheroRepository.save(new Superhero(name, pseudonym, publisher));
        return new SuperheroResponse(saved);
    }

    @Transactional
    public SuperheroResponse edit(Long id, String name, String pseudonym, Publisher publisher) {
        Utils.checkParam(id, "Id cannot be empty.");
        Superhero superhero = getSuperhero(id);
        if (!Utils.isEmptyString(name)) {
            superhero.setName(name);
        }
        if (Utils.isEmptyString(pseudonym)) {
            superhero.setPseudonym(null);
        } else {
            superhero.setPseudonym(pseudonym);
        }
        if (publisher != null) {
            superhero.setPublisher(publisher);
        }
        return new SuperheroResponse(superhero);
    }

    @Transactional
    public void deleteSuperhero(Long id) {
        Utils.checkParam(id, "Id cannot be empty.");
        superheroRepository.deleteById(id);
    }

    @Transactional
    public SuperheroResponse addSkills(Long id, Set<Skill> skills) {
        Utils.checkParam(id, "Id cannot be empty.");
        Superhero superhero = getSuperhero(id);
        superhero.getSkills().addAll(skills);
        return new SuperheroResponse(superhero);
    }

    @Transactional
    public SuperheroResponse deleteSkills(Long id, Set<Skill> skills) {
        Utils.checkParam(id, "Id cannot be empty.");
        Superhero superhero = getSuperhero(id);
        superhero.getSkills().removeAll(skills);
        return new SuperheroResponse(superhero);
    }

    @Transactional
    public SuperheroResponse addAllies(Long id, Set<Long> allyIds) {
        Utils.checkParam(id, "Id cannot be empty.");
        Set<Superhero> allies = new HashSet<>();
        superheroRepository.findAllById(allyIds).forEach(allies::add);
        Superhero superhero = getSuperhero(id);
        superhero.getAllies().addAll(allies);
        superhero.getImplicitAllies().addAll(allies);
        return new SuperheroResponse(superhero);
    }

    @Transactional
    public SuperheroResponse deleteAllies(Long id, Set<Long> allyIds) {
        Utils.checkParam(id, "Id cannot be empty.");
        Superhero superhero = getSuperhero(id);
        superhero.getAllies().removeIf(s -> allyIds.contains(s.getSuperheroId()));
        superhero.getImplicitAllies().removeIf(s -> allyIds.contains(s.getSuperheroId()));
        return new SuperheroResponse(superhero);
    }

    public SuperheroResponse find(Long id) {
        Utils.checkParam(id, "Id cannot be empty.");
        Optional<Superhero> superheroOptional = superheroRepository.findById(id);
        if (superheroOptional.isPresent()) {
            return new SuperheroResponse(superheroOptional.get());
        } else {
            throw new EntityNotFoundException(String.format("No superhero found for id = %s.", id));
        }
    }

    public List<SuperheroResponse> findPage(Integer pageNum, Integer pageSize) {
        Page<Superhero> superheroes = superheroRepository.findAll(
                PageRequest.of(checkPageNum(pageNum), checkPageSize(pageSize),
                        new Sort(Sort.Direction.ASC, "name")));
        return superheroes.stream().map(SuperheroResponse::new)
                .collect(Collectors.toList());
    }

    private static int checkPageNum(Integer pageNum) {
        return pageNum == null || pageNum < 0 ? DEFAULT_PAGE_NUM : pageNum;
    }

    private static int checkPageSize(Integer pageSize) {
        return pageSize == null || pageSize < 0 ? DEFAULT_PAGE_SIZE : pageSize;
    }

    private Superhero getSuperhero(long id) {
        Optional<Superhero> optional = superheroRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            throw new NoSuchEntityException(String.format(
                    "No superhero with id = %s.", id));
        }
    }

}
